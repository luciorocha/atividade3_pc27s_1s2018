/**
 * Exemplo1: Programacao com threads
 * Autor: Lucio Agostinho Rocha
 * Ultima modificacao: 07/08/2017
 */
package atividade3;

import java.util.Random;

/**
 *
 * @author Lucio
 */
public class Exemplo3  {

        public static void main(String [] args){
            
            System.out.println("Inicio da criacao das threads.");
            
            //Cria cada thread com um novo runnable selecionado
            Thread t1 = new Thread(new PrintTasks("thread1"));            
            
            //Inicia as threads, e as coloca no estado EXECUTAVEL
            t1.start(); //invoca o método run de t1
            
            System.out.println("Threads criadas");
        }
        
}
